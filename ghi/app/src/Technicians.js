import React, { useEffect, useState } from "react";

const Technicians = () => {
  const [technicians, setTechnicians] = useState([]);

  const getTechnicians = async () => {
    const url = "http://localhost:8080/api/technicians/";
    const req = await fetch(url);

    if (req.ok) {
      const techniciansList = await req.json();
      setTechnicians(techniciansList.technicians);
    }
  };

  useEffect(() => {
    getTechnicians();
  }, []);
  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians &&
            technicians.map((technician) => (
              <tr key={technician.employee_id}>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
                <td>{technician.employee_id}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
};

export default Technicians;
