import React, { useEffect, useState } from 'react';

export default function VehicleModelList() {
    const [models, setModels] = useState([]);

    const getModels = async (id) => {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        };
    };

    useEffect(() => {
        getModels();
    }, []);

    return(
        <div>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={ model.id }>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td>
                                    <img src={ model.picture_url }
                                        alt="model"
                                        width="400"
                                        height="250"
                                    />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};
