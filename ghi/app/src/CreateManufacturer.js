import React, { useState } from "react";

const CreateManufacturer = () => {
  const [formData, setFormData] = useState({
    name: "",
  });

  const handleChange = (event) => {
    const name = event.target.name;
    const input = event.target.value;

    setFormData({
      ...formData,
      [name]: input,
    });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8100/api/manufacturers/";

    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-type": "application/json",
      },
    };
    const req = await fetch(url, fetchOptions);
    if (req.ok) {
      alert("Manufacturer created successfully!");
      setFormData({
        name: "",
      });
    }
  };
  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1>Create a Manufacturer</h1>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Create a Manufacturer
          </label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Create a Manufacturer"
            onChange={handleChange}
            value={formData.name}
            name="name"
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </>
  );
};

export default CreateManufacturer;
