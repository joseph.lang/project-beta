import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturerList from "./ManufacturerList";
import AutomobileList from "./AutomobileList";
import VehicleModelList from "./VehicleModelList";
import CreateTechnician from "./CreateTechnician";
import Technicians from "./Technicians";
import CreateServiceAppointment from "./CreateServiceAppointment";
import ServiceHistory from "./ServiceHistory";
import ServiceAppointments from "./ServiceAppointments";
import CreateManufacturer from "./CreateManufacturer";
import CreateModel from "./CreateModel";
import CreateAutomobile from "./CreateAutomobile";
import CreateSalesperson from "./CreateSalesperson";
import SalespeopleList from "./SalespeopleList";
import CreateCustomer from "./CreateCustomer";
import CustomerList from "./CustomerList";
import CreateSales from "./CreateSales";
import SalesList from "./SalesList";
import SalespersonHistory from "./SalespersonHistory";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="models" element={<VehicleModelList />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="/technicians/create" element={<CreateTechnician />} />
          <Route path="/technicians" element={<Technicians />} />
          <Route
            path="/appointments/create"
            element={<CreateServiceAppointment />}
          />
          <Route path="/appointments/history" element={<ServiceHistory />} />
          <Route path="/appointments" element={<ServiceAppointments />} />
          <Route
            path="/manufacturers/create"
            element={<CreateManufacturer />}
          />
          <Route path="/models/create" element={<CreateModel />} />
          <Route path="/automobiles/create" element={<CreateAutomobile />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/salespeople/create" element={<CreateSalesperson />} />
          <Route path="/salespeople/" element={<SalespeopleList />} />
          <Route path="/customer/create" element={<CreateCustomer />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/sales/create" element={<CreateSales />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/salesperson/history" element={<SalespersonHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
