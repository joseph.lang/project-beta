from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from datetime import datetime
# Create your views here.

class TechnicianEncoder(ModelEncoder):
  model = Technician
  properties = ["first_name", "last_name", "employee_id"]

class AutomobileVOEncoder(ModelEncoder):
   model = AutomobileVO
   properties = ["vin", "sold"]

class AppointmentEncoder(ModelEncoder):
   model = Appointment
   properties = ["date_time", "reason", "status", "vin", "customer", "technician", "id"]

   def default(self, obj):
    if isinstance(obj, Technician):
        return {"first_name": obj.first_name, "last_name": obj.last_name}
    return super().default(obj)


@require_http_methods(["GET", "POST"])
def api_list_technicians(req):
  if req.method == "GET":
      technicians = Technician.objects.all()
      return JsonResponse(
        {"technicians": technicians},
        encoder=TechnicianEncoder
      )
  else:
    try:
     technician = json.loads(req.body)
     Technician.objects.create(**technician)
     return JsonResponse(
        "Technician created successfully!",
        encoder=TechnicianEncoder,
        safe=False
        )
    except Technician.DoesNotExist:
       return JsonResponse('Technician does not exist.', status=404)

@require_http_methods(["GET", "POST"])
def api_list_appointments(req):
   if req.method == "GET":
      appointments = Appointment.objects.all()
      return JsonResponse(
         {"appointments": appointments},
         encoder=AppointmentEncoder,
         safe=False
      )
   else:
      try:
        appointment = json.loads(req.body)
        technician_id = appointment.get("technician")
        technician = Technician.objects.get(employee_id=technician_id)
        appointment['technician'] = technician

        date = appointment.get("date")
        time = appointment.get("time")
        date_time_str = f"{date} {time}"
        date_time = datetime.strptime(date_time_str, '%Y-%m-%d %H:%M')
        appointment["date_time"] = date_time
        appointment.pop("date")
        appointment.pop("time")

        Appointment.objects.create(**appointment)
        return JsonResponse(
          "Appointment successfully created!",
          encoder=AppointmentEncoder,
          safe=False
        )
      except Technician.DoesNotExist:
         return JsonResponse("Technician does not exist.", status=404)

@require_http_methods(["PUT"])
def set_appointment_cancelled(req, id):
   try:
    appointment = Appointment.objects.get(id=id)
    appointment.status = "cancelled"
    appointment.save()
    return JsonResponse(
        "Appointment cancelled successfully!",
        safe=False
    )
   except Appointment.DoesNotExist:
      return JsonResponse (
         'Appointment does not exist.', status=404
      )
@require_http_methods(["PUT"])
def set_appointment_finished(req, id):
   try:
    appointment = Appointment.objects.get(id=id)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse(
        "Appointment successfully marked as finished!",
        safe=False
    )
   except Appointment.DoesNotExist:
      return JsonResponse (
         'Appointment does not exist.', status=404
      )

@require_http_methods(["GET"])
def get_vins(req):
   vo_data = AutomobileVO.objects.all()
   vins = []
   for item in vo_data:
      vins.append(item.vin)
   return JsonResponse(
      {"vins": vins},
      encoder=AutomobileVOEncoder
      )
